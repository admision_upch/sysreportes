@extends('backLayout.app')
@section('title2')
    <h1>Facultades <a href="{{ url('facultades/create') }}" class="btn btn-primary pull-right btn-sm">Agregar Nueva Facultad</a></h1>
@endsection

@section('content2')
    <hr/>
    <div class="table ">
        <table class="table table-bordered table-striped table-hover" id="tblfacultades">
            <thead>
                <tr>
                    <th>ID</th><th>Id Dependencia</th><th>Cod Dependencia</th><th>Name</th><th>Modalidad</th><th>Formacion</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($facultades as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('facultades', $item->id) }}">{{ $item->id_dependencia }}</a></td><td>{{ $item->cod_dependencia }}</td><td>{{ $item->name }}</td>
                    <td>{{ $item->modalidad->name }}</td>
                    <td>{{ $item->formacion->name }}</td>
                    <td>
                        <a href="{{ url('facultades/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['facultades', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function(){
        
        $('#tblfacultades').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection