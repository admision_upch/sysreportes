@extends('backLayout.app')
@section('title')
Sistema
@stop

@section('content')

    <h1>Sistema</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Url</th><th>Abreviatura</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $sistema->id }}</td> <td> {{ $sistema->name }} </td><td> {{ $sistema->url }} </td><td> {{ $sistema->abreviatura }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection