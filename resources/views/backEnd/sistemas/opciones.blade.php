@extends('backLayout.app')
@section('css2')
<link href="{{ asset('/assets/global/plugins/jstree/dist/themes/default/style.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('title2')
<h1>Editar Opciones</h1>
@endsection
@section('content2')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">{{$sistema->name}}</span>
        </div>

    </div>
    <div class="portlet-body">
         <a href="javascript:;" id="firstname" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-original-title="Enter your firstname"> </a>
        <div id="jstree_opciones" class="tree-demo"> </div>
    </div>
</div>

<hr/>
@endsection
@section('js2')
<script src="{{ asset('/assets/global/plugins/jstree/dist/jstree.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js') }}" type="text/javascript"></script>

<script type="text/javascript">

$(function () {

$('#jstree_opciones').jstree({
'core': {
'data': {
"url": "{{ route('opciones_tree', ['id' => $sistema->id]) }}",
        'data': function (node) {
        return {'id': node.id};
        }
},
        'check_callback': true,
        'themes': {
        'responsive': false
        }
},
        'force_text': true,
        'plugins': ['state', 'dnd', 'contextmenu', 'wholerow']
        })
        .on('delete_node.jstree', function (e, data) {
           $.post("{{ route('opciones_tree_crud_delete_node')}}",
        {
               id: data.node.id,
                "_token": "{{ csrf_token() }}"
        }) .done(function (d) {
            
               // data.instance.refresh();
                })
               .fail(function () {
                data.instance.refresh();
                });
        })
        .on('create_node.jstree', function (e, data) {
        $.post("{{ route('opciones_tree_crud_create_node')}}",
        {
                sistema_id: {{$sistema->id}},
                padre: data.node.parent,
                orden: data.position,
                name: data.node.text,
                "_token": "{{ csrf_token() }}"
        })
                .done(function (d) {
                data.instance.set_id(data.node, d.id);
                })
                .fail(function () {
                data.instance.refresh();
                });
        })
        .on('rename_node.jstree', function (e, data) {
            
              $.post("{{ route('opciones_tree_crud_rename_node')}}",
        {
                
                id: data.node.id,
                name: data.node.text,
                "_token": "{{ csrf_token() }}"
        }) .done(function (d) {
            
               // data.instance.refresh();
                })
               .fail(function () {
                data.instance.refresh();
                });
        })
        .on('move_node.jstree', function (e, data) {
        $.get('?operation=move_node', {'id': data.node.id, 'parent': data.parent, 'position': data.position})
                .fail(function () {
                data.instance.refresh();
                });
        })
        .on('copy_node.jstree', function (e, data) {
        $.get('?operation=copy_node', {'id': data.original.id, 'parent': data.parent, 'position': data.position})
                .always(function () {
                data.instance.refresh();
                });
        })
        .on('changed.jstree', function (e, data) {
          
        if (data && data.selected && data.selected.length) {
        $.get('?operation=get_content&id=' + data.selected.join(':'), function (d) {
        $('#data .default').text(d.content).show();
        });
        } else {
        $('#data .content').hide();
        $('#data .default').text('Select a file from the tree.').show();
        }
        });

});

</script>

@endsection


