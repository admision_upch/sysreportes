@extends('backLayout.app')
@section('title2')
<h1>Crear Nuevo Sistema</h1>
@endsection

@section('content2')
<hr/>
{!! Form::open(['url' => 'sistemas',  'class' => 'form-horizontal']) !!}

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label', 'data-required'=>'1']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control' , 'required'=>'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('url') ? 'has-error' : ''}}">
    {!! Form::label('url', 'Url: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('url', null, ['class' => 'form-control' , 'required'=>'required']) !!}
        {!! $errors->first('url', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('abreviatura') ? 'has-error' : ''}}">
    {!! Form::label('abreviatura', 'Abreviatura: ', ['class' => 'col-sm-3 control-label', 'required'=>'required']) !!}
    <div class="col-sm-6">
        {!! Form::text('abreviatura', null, ['class' => 'form-control']) !!}
        {!! $errors->first('abreviatura', '<p class="help-block">:message</p>') !!}
    </div>
</div>


@include('componentes.submit_reset_form_create')

{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif
@endsection

