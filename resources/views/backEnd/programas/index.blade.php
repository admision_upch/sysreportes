@extends('backLayout.app')
@section('title2')
   <h1>Programas <a href="{{ url('programas/create') }}" class="btn btn-primary pull-right btn-sm">Agregar Nuevo Programa</a></h1>
@endsection

@section('content2')
    <hr/>
    <div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblprogramas">
        <thead>
            <tr>
                <th>ID</th><th>Cod.</th><th>Programa</th><th>Modalidad</th><th>Formacion</th><th>Facultad</th><th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($programas as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td><a href="{{ url('programas', $item->id) }}">{{ $item->cod }}</a></td><td>{{ $item->name }}</td><td>{{ $item->modalidad->name }}</td><td>{{ $item->formacion->name }}</td>

                <td>{{ $item->facultad->name }}</td>
                <td>
                    <a href="{{ url('programas/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                    {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['programas', $item->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>

@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function(){
        
        $('#tblprogramas').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection