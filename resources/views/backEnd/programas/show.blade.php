@extends('backLayout.app')
@section('title')
Programa
@stop

@section('content')

    <h1>Programa</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Id Dependencia</th><th>Modalidad Id</th><th>Formacion Id</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $programa->id }}</td> <td> {{ $programa->id_dependencia }} </td><td> {{ $programa->modalidad_id }} </td><td> {{ $programa->formacion_id }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection