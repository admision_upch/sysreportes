@extends('backLayout.app')
@section('title2')
<h1>Crear Nuevo Programa</h1>
@endsection

@section('content2')
<hr/>

{!! Form::open(['url' => 'programas', 'class' => 'form-horizontal']) !!}
<div class="form-group {{ $errors->has('modalidad_id') ? 'has-error' : ''}}">
    {!! Form::label('modalidad_id', 'Modalidad: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectModalidades!!}
        {!! $errors->first('modalidad_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('formacion_id') ? 'has-error' : ''}}">
    {!! Form::label('formacion_id', 'Formacion: ', ['class' => 'col-sm-3 control-label ']) !!}
    <div class="col-sm-6">
        {!! $selectFormaciones!!}
        {!! $errors->first('formacion_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('id_dependencia') ? 'has-error' : ''}}">
    {!! Form::label('id_dependencia', 'Facultad: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectFacultades!!}
        {!! $errors->first('id_dependencia', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('cod') ? 'has-error' : ''}}">
    {!! Form::label('cod', 'Código: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('cod', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('cod', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Nombre: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>


@include('componentes.submit_reset_form_create')
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif


@endsection

@section('js2')
<script type="text/javascript">

    $(document).ready(function () {
        
        $('#formacion_id').change(function () {
            var modalidad_id = $("#modalidad_id").val();
            var formacion_id = $("#formacion_id").val();
            $.ajax({
                type: "GET",
                url: "/facultadesselect/modalidad/" + modalidad_id + "/formacion/" + formacion_id,
                success: function (data) {
                    //console.log(data);
                    $('#id_dependencia').html(data);
                }
            })
        });


    });



</script>
@endsection

