@extends('backLayout.app')
@section('title2')
<h1>Accesos </h1>
@endsection

@section('content2')
<hr/>
<div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblaccesos">
        <thead>
            <tr>
                <th>ID</th><th>Sistema</th><th>Perfil</th><th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($perfiles as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->sistema->name }}</td>
                <td>{{ $item->name }}</td>
                <td>
                    <a href="{{ url('accesos/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Ver Accesos</a> 
                   
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tblaccesos').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: false,
                    searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection