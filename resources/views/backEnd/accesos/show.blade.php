@extends('backLayout.app')
@section('title')
Acceso
@stop

@section('content')

    <h1>Acceso</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Perfil Id</th><th>Opcion Id</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $acceso->id }}</td> <td> {{ $acceso->perfil_id }} </td><td> {{ $acceso->opcion_id }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection