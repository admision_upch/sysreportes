@extends('backLayout.app')
@section('css2')
<link href="{{ asset('/assets/global/plugins/jstree/dist/themes/default/style.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('title2')
<h1>Editar Acceso</h1>
@endsection

@section('content2')

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bubble font-green-sharp"></i>
            <span class="caption-subject font-green-sharp bold uppercase">Checkable Tree</span>
        </div>
        <div class="actions">
            <div class="btn-group">
                <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:;"> Option 1</a>
                    </li>
                    <li class="divider"> </li>
                    <li>
                        <a href="javascript:;">Option 2</a>
                    </li>
                    <li>
                        <a href="javascript:;">Option 3</a>
                    </li>
                    <li>
                        <a href="javascript:;">Option 4</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div id="jstree_accesos" class="tree-demo"> </div>
    </div>
</div>

<hr/>
{!! Form::model($acceso, [
'method' => 'PATCH',
'url' => ['accesos', $acceso->id],
'class' => 'form-horizontal'
]) !!}

<div class="form-group {{ $errors->has('perfil_id') ? 'has-error' : ''}}">
    {!! Form::label('perfil_id', 'Perfil Id: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectPerfiles !!}
        {!! $errors->first('perfil_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('opcion_id') ? 'has-error' : ''}}">
    {!! Form::label('opcion_id', 'Opcion Id: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectOpciones !!}
        {!! $errors->first('opcion_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>


@include('componentes.submit_reset_form_update')
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif


@endsection

@section('js2')
<script src="{{ asset('/assets/global/plugins/jstree/dist/jstree.min.js') }}" type="text/javascript"></script>






<script type="text/javascript">

$(function () {
    tree();

});

function tree() {

    $('#jstree_accesos').jstree({
        "core": {
            "animation": 0,
            "check_callback": true,
            "themes": {"stripes": true},
            "data": {
                "url": "{{ route('accesos_tree') }}",
                "dataType": "json",
                "data": function (node) {
                    return {"id": node.id};
                }
            }
        },
        "types": {
            "#": {
                "max_children": 1,
                "max_depth": 4,
                "valid_children": ["root"]
            },
            "root": {
                "icon": "/static/3.3.4/assets/images/tree_icon.png",
                "valid_children": ["default"]
            },
            "default": {
                "valid_children": ["default", "file"]
            },
            "file": {
                "icon": "glyphicon glyphicon-file",
                "valid_children": []
            }
        },
        "plugins": [
            "contextmenu", "dnd", "search",
            "state", "types", "wholerow", "json_data", "ui", "crrm", "themes", "html_data"
        ],
        "contextmenu": {
            "items": function ($node) {
                var tree = $("#jstree_demo").jstree(true);
                return {
                    "Create": {
                        "separator_before": false,
                        "separator_after": false,
                        "label": "Crear",
                        "action": function (obj) {
                            addUnidadOrganica($node);
                        }
                    },
                    /*   "Rename": {
                     "separator_before": false,
                     "separator_after": false,
                     "label": "Rename",
                     "action": function (obj) {
                     tree.edit($node);
                     }
                     },*/
                    "Update": {
                        "separator_before": false,
                        "separator_after": false,
                        "label": "Editar",
                        "action": function (obj) {
                            updUnidadOrganica($node);
                        }
                    }
                };
            }
        }


    });



}
function addUnidadOrganica($node)
{
    alert('add');
    var text = $node['text'];
    $.colorbox({width: "50%", height: "50%", iframe: true, escKey: true, overlayClose: true, href: "/sisSeguridad/SPrincipal?pAccion=aUnidadOrganica&accion=NUEVO&idpadre=" + $node.id + "&text=" + text});
}
function updUnidadOrganica($node)
{
    alert('update');
    var text = $node['text'];
    $.colorbox({width: "50%", height: "50%", iframe: true, escKey: true, overlayClose: true, href: "/sisSeguridad/SPrincipal?pAccion=aUnidadOrganica&accion=MODIF&idpadre=" + $node.id + "&text=" + text + "&parent=" + $node.parent});
}





</script>

@endsection


