@extends('backLayout.app')
@section('title2')
<h1>Crear Nuevo Acceso</h1>
@endsection

@section('content2')
<hr/>

{!! Form::open(['url' => 'accesos', 'class' => 'form-horizontal']) !!}
<div class="form-group {{ $errors->has('sistema_id') ? 'has-error' : ''}}">
    {!! Form::label('sistema_id', 'Sistema: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectSistemas !!}
        {!! $errors->first('sistema_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('perfil_id') ? 'has-error' : ''}}">
    {!! Form::label('perfil_id', 'Perfil: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectPerfiles !!}
        {!! $errors->first('perfil_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('opcion_id') ? 'has-error' : ''}}">
    {!! Form::label('opcion_id', 'Opcion: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! $selectOpciones !!}
        {!! $errors->first('opcion_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>


@include('componentes.submit_reset_form_create')
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif

@endsection