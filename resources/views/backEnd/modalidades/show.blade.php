@extends('backLayout.app')
@section('title')
Modalidade
@stop

@section('content')

    <h1>Modalidade</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $modalidade->id }}</td> <td> {{ $modalidade->name }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection