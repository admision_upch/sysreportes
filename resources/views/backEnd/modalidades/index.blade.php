@extends('backLayout.app')
@section('title')
Modalidade
@stop

@section('content')

    <h1>Modalidades <a href="{{ url('modalidades/create') }}" class="btn btn-primary pull-right btn-sm">Add New Modalidade</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblmodalidades">
            <thead>
                <tr>
                    <th>ID</th><th>Name</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($modalidades as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td><a href="{{ url('modalidades', $item->id) }}">{{ $item->name }}</a></td>
                    <td>
                        <a href="{{ url('modalidades/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['modalidades', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblmodalidades').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection