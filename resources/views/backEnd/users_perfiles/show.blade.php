@extends('backLayout.app')
@section('title')
Users_perfile
@stop

@section('content')

    <h1>Users_perfile</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Perfil Id</th><th>User Id</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $users_perfile->id }}</td> <td> {{ $users_perfile->perfil_id }} </td><td> {{ $users_perfile->user_id }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection