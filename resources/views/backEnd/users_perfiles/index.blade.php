@extends('backLayout.app')
@section('title')
Users_perfile
@stop

@section('content')

    <h1>Users_perfiles <a href="{{ url('users_perfiles/create') }}" class="btn btn-primary pull-right btn-sm">Add New Users_perfile</a></h1>
    <div class="table table-responsive">
        <table class="table table-bordered table-striped table-hover" id="tblusers_perfiles">
            <thead>
                <tr>
                    <th>ID</th><th>Sistema</th><th>Perfil</th><th>Usuario</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($users_perfiles as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->perfil->sistema->name }}</td>
                    <td><a href="{{ url('users_perfiles', $item->id) }}">{{ $item->perfil->name }}</a></td><td>{{ $item->user->name }}</td>
                    <td>
                        <a href="{{ url('users_perfiles/' . $item->id . '/edit') }}" class="btn btn-primary btn-xs">Update</a> 
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['users_perfiles', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#tblusers_perfiles').DataTable({
            columnDefs: [{
                targets: [0],
                visible: false,
                searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection