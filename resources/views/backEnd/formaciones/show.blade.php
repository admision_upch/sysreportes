@extends('backLayout.app')
@section('title')
Formacione
@stop

@section('content')

    <h1>Formacione</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $formacione->id }}</td> <td> {{ $formacione->name }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection