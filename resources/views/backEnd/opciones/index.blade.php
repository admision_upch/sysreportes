@extends('backLayout.app')
@section('title2')
<h1>Opciones </h1>
@endsection

@section('content2')
<hr/>
<div class="table ">
    <table class="table table-bordered table-striped table-hover" id="tblsistemas">
        <thead>
            <tr>
                <th>ID</th><th>Sistema</th><th>Abreviatura Sistema</th><th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sistemas as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td><td>{{ $item->abreviatura }}</td>
                <td>
                    <a href="{{ url('sistemas/' . $item->id . '/opciones') }}" class="btn btn-primary btn-xs">Opciones</a> 
                  
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('js2')
<script type="text/javascript">
    $(document).ready(function () {

        $('#tblsistemas').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: false,
                    searchable: false
                },
            ],
            order: [[0, "asc"]],
        });
    });
</script>
@endsection