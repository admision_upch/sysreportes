@extends('backLayout.app')
@section('title2')
<h1>Editar Opción</h1>
@endsection

@section('content2')
<hr/>
{!! Form::model($opcione, [
'method' => 'PATCH',
'url' => ['opciones', $opcione->id],
'class' => 'form-horizontal'
]) !!}

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('enlace') ? 'has-error' : ''}}">
    {!! Form::label('enlace', 'Enlace: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('enlace', null, ['class' => 'form-control']) !!}
        {!! $errors->first('enlace', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('destino') ? 'has-error' : ''}}">
    {!! Form::label('destino', 'Destino: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('destino', null, ['class' => 'form-control']) !!}
        {!! $errors->first('destino', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('nivel') ? 'has-error' : ''}}">
    {!! Form::label('nivel', 'Nivel: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('nivel', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nivel', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('tipoenlace') ? 'has-error' : ''}}">
    {!! Form::label('tipoenlace', 'Tipoenlace: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('tipoenlace', null, ['class' => 'form-control']) !!}
        {!! $errors->first('tipoenlace', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('padre') ? 'has-error' : ''}}">
    {!! Form::label('padre', 'Padre: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('padre', null, ['class' => 'form-control']) !!}
        {!! $errors->first('padre', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('orden') ? 'has-error' : ''}}">
    {!! Form::label('orden', 'Orden: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('orden', null, ['class' => 'form-control']) !!}
        {!! $errors->first('orden', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('imagen') ? 'has-error' : ''}}">
    {!! Form::label('imagen', 'Imagen: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('imagen', null, ['class' => 'form-control']) !!}
        {!! $errors->first('imagen', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('sistema_id') ? 'has-error' : ''}}">
    {!! Form::label('sistema_id', 'Sistema: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">


        {!! $selectSistemas !!}
        {!! $errors->first('sistema_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@include('componentes.submit_reset_form_update')
{!! Form::close() !!}

@if ($errors->any())
<ul class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif


@endsection