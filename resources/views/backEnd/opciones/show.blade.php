@extends('backLayout.app')
@section('title')
Opcione
@stop

@section('content')

    <h1>Opcione</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Enlace</th><th>Destino</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $opcione->id }}</td> <td> {{ $opcione->name }} </td><td> {{ $opcione->enlace }} </td><td> {{ $opcione->destino }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection