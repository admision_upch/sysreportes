@extends('backLayout.app')
@section('title')
Perfile
@stop

@section('content')

    <h1>Perfile</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Estado</th><th>Sistema Id</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $perfile->id }}</td> <td> {{ $perfile->name }} </td><td> {{ $perfile->estado }} </td><td> {{ $perfile->sistema_id }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection