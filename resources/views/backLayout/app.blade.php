@extends ('layouts.index')    
@section('css')
    @yield('css2')
@endsection 
@section('content')   
<div class="m-heading-1 border-green m-bordered">
    <div class="portlet-title">
        <div class="caption font-dark">
            <span class="caption-subject bold uppercase">
                @yield('title2')
            </span>
        </div>
        <div class="tools"> </div>
    </div>
    <div class="portlet-body">

        @yield('content2')
        
    </div>
</div>
@section('js')
    @yield('js2')
@endsection
@endsection


