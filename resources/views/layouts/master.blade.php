<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <title>Universidad Peruana Cayetano Heredia - Admisión</title>

           
        <link rel="stylesheet" href="{{ asset('css/vendor.bundle.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.bundle.css') }}">
        <link rel="stylesheet" href="{{ asset('css/theme-a.css') }}">
        @yield('css')

    </head>

    <body>
        <div id="app_wrapper">
            @include('layouts.header')
            @include('layouts.aside1')
            <section id="content_outer_wrapper">
                
                @yield('content')
                                
                @include('layouts.footer')
            </section>
            @include('layouts.aside2')
        </div>
        <script src="{{ asset('js/vendor.bundle.js') }}"></script>
        <script src="{{ asset('js/app.bundle.js') }}"></script>
        @yield('js')
    </body>

</html>
