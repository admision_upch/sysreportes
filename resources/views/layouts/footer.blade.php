      <div class="page-footer">
            <div class="page-footer-inner"> 2017 &copy; Oficina de Admisión
                <a target="_blank" href="http://www.cayetano.edu.pe/cayetano/es/">Oficina de Admisión</a> &nbsp;|&nbsp;
                <a href="http://www.cayetano.edu.pe/cayetano/es/" title="" target="_blank">Universidad Peruana Cayetano Heredia</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>