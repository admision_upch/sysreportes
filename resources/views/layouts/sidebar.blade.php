<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

            <li class="heading">
                <h3 class="uppercase">Principal</h3>
            </li>
            <li class="nav-item  ">
                <a href="{{url('home')}}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Inicio</span>

                </a>

            </li>
            <li class="heading">
                <h3 class="uppercase">Mantenimiento</h3>
            </li>
            <li class="nav-item  ">
                <a href="{{ url('sistemas') }}"  class="nav-link nav-toggle">
                    <i class="icon-shuffle"></i>
                    <span class="title">Sistemas</span>

                </a>

            </li>
            <li class="nav-item  ">
                <a href="{{ url('perfiles') }}" class="nav-link nav-toggle">
                    <i class="icon-picture"></i>
                    <span class="title">Perfiles</span>

                </a>

            </li>
            <li class="nav-item  ">
                <a href="{{ url('opciones') }}" class="nav-link nav-toggle">
                    <i class="icon-bag"></i>
                    <span class="title">Opciones</span>

                </a>

            </li>

            <li class="nav-item  ">
                <a href="{{ url('accesos') }}" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">Accesos</span>

                </a>

            </li>

            <li class="nav-item  ">
                <a href="{{ url('users') }}" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">Usuarios</span>

                </a>

            </li>
            <li class="nav-item  ">
                <a href="/" class="nav-link nav-toggle">
                    <i class="icon-key"></i>
                    <span class="title">Asignación de Perfiles</span>

                </a>

            </li>

            <li class="nav-item  ">
                <a href="{{ url('facultades') }}" class="nav-link nav-toggle">
                    <i class="icon-key"></i>
                    <span class="title">Facultades</span>

                </a>

            </li>

            <li class="nav-item  ">
                <a href="{{ url('programas') }}" class="nav-link nav-toggle">
                    <i class="icon-key"></i>
                    <span class="title">Programas</span>

                </a>

            </li>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>