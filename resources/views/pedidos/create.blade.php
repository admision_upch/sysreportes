@extends ('layouts.master')         


@section('content')
<div id="content_wrapper" class="simple">
    <div id="header_wrapper" class="header-sm">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <header id="header">
                        <h1>Pedidos de Carnés</h1>
                        <ol class="breadcrumb">
                            <li><a href="index.html">Dashboard</a></li>

                        </ol>
                    </header>
                </div>
            </div>
        </div>
    </div>
    <div id="content" class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="content">
                    <div class="content-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="card" id="sweet_alerts_card">
                                    <header class="card-heading ">
                                        <h2 class="card-title">Creación de Pedidos de Carnés </h2>
                                    </header>
                                    <div class="card-body">
                    
                                        
                                        
                                        <div class="card" id="rootwizard">
                        <div class="card-heading">
                            <form class="form floating-label">
                                <div class="form-wizard-nav">
                                    <div class="progress" style="width: 75%;">
                                        <div class="progress-bar" style="width:0%;"></div>
                                    </div>
                                    <ul class="nav nav-justified nav-pills">
                                        <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true"><span class="step">1</span> <span class="title">Importación de archivos SUNEDU</span></a></li>
                                        <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false"><span class="step">2</span> <span class="title">Normalización de Estudiantes</span></a></li>

                                    </ul>
                                </div>
                            </form>
                        </div>
                        <div class="card-body p-0">
                            <div class="form-wizard form-wizard-horizontal">
                                <div class="tab-content clearfix p-30">
                                    <div class="tab-pane active" id="tab1">



                                        <div class="modal fade" id="toolabr_modal" tabindex="-1" role="dialog" aria-labelledby="toolabr_modal">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="card m-0">
                                                        <header class="card-heading p-b-20">
                                                            <h2 class="card-title">Toolbar</h2>
                                                            <div class="card-search">
                                                                <div class="form-group is-empty">
                                                                    <a href="javascript:void(0)" class="close-search" data-card-search="close" data-toggle="tooltip" data-placement="top" title="" data-original-title="Back"> <i class="zmdi zmdi-arrow-left"></i></a>
                                                                    <input type="text" placeholder="Search and press enter..." class="form-control" autocomplete="off">
                                                                    <a href="javascript:void(0)" class="clear-search" data-card-search="clear" data-toggle="tooltip" data-placement="top" title="" data-original-title="Clear search"><i class="zmdi zmdi-close-circle"></i></a>
                                                                </div>
                                                            </div>
                                                            <ul class="card-actions icons right-top">
                                                                <li>
                                                                    <a href="javascript:void(0)" data-card-search="open">
                                                                        <i class="zmdi zmdi-search"></i>
                                                                    </a>
                                                                </li>
                                                                <li class="dropdown">
                                                                    <a href="javascript:void(0)" data-toggle="dropdown">
                                                                        <i class="zmdi zmdi-more-vert"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu btn-primary dropdown-menu-right">
                                                                        <li>
                                                                            <a href="javascript:void(0)">Option One</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:void(0)">Option Two</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="javascript:void(0)">Option Three</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close">
                                                                        <i class="zmdi zmdi-close"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </header>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in ligula id sem tristique ultrices eget id neque. Duis enim turpis, tempus at accumsan vitae, lobortis id sapien. Pellentesque nec orci mi, in pharetra ligula. Nulla facilisi. Nulla
                                                            facilisi. Mauris convallis venenatis massa, quis consectetur felis ornare quis. Sed aliquet nunc ac ante molestie ultricies. Nam pulvinar ultricies bibendum.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
                                                        <button type="button" class="btn btn-primary">Ok</button>
                                                    </div>
                                                </div>
                                                <!-- modal-content -->
                                            </div>
                                            <!-- modal-dialog -->
                                        </div>


                                        <h3>Paso 1 - Importación de archivos SUNEDU</h3>
                                        <div>
                                            <form>

                                                <div class="form-group is-empty">
                                                    <label for="exampleInputFile">Archivo Facultades - Sunedu</label>
                                                    <div class="input-group">
                                                        <input type="file" class="form-control" placeholder="File Upload..." multiple>
                                                        <div class="input-group">
                                                            <input type="text" readonly="" class="form-control" placeholder="Archivo Facultades - Sunedu">
                                                            <span class="input-group-btn input-group-sm">
                                                                <button type="button" class="btn btn-primary btn-sm">
                                                                    Adjuntar
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <small id="fileHelp" class="form-text text-muted">Archivo que contiene las facultades indicadas por la SUNEDU. Descargar de SUNEDU(https://www.carneuniversitario.com.pe), ir a la opción Ayuda -> Carga Masiva de Estudiantes 1.0 Sunedu_CargaMasiva, luego click en Install para instalar el aplicactivo de Carga Masiva. Ingresar al aplicativo y descargar Plantilla. (Hoja N° 3 del archivo, se encuentran las facultades ) </small>
                                                </div>



                                                <div class="form-group is-empty">
                                                    <label for="exampleInputFile">Archivo Carreras - Sunedu</label>
                                                    <div class="input-group">
                                                        <input type="file" class="form-control" placeholder="File Upload..." multiple>
                                                        <div class="input-group">
                                                            <input type="text" readonly="" class="form-control" placeholder="Archivo Facultades - Sunedu">
                                                            <span class="input-group-btn input-group-sm">
                                                                <button type="button" class="btn btn-primary btn-sm">
                                                                    Adjuntar
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <small id="fileHelp" class="form-text text-muted">Archivo que contiene las carreras indicadas por la SUNEDU. Descargar de SUNEDU(https://www.carneuniversitario.com.pe), ir a la opción Ayuda -> Carga Masiva de Estudiantes 1.0 Sunedu_CargaMasiva, luego click en Install para instalar el aplicactivo de Carga Masiva. Ingresar al aplicativo y descargar Plantilla. (Hoja N° 3 del archivo, se encuentran las carreras )</small>
                                                </div>



                                                <div class="form-group is-empty">
                                                    <label for="exampleInputFile">Archivo Estudiantes - Sunedu</label>
                                                    <div class="input-group">
                                                        <input type="file" class="form-control" placeholder="File Upload..." multiple>
                                                        <div class="input-group">
                                                            <input type="text" readonly="" class="form-control" placeholder="Archivo Facultades - Sunedu">
                                                            <span class="input-group-btn input-group-sm">
                                                                <button type="button" class="btn btn-primary btn-sm">
                                                                    Adjuntar
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <small id="fileHelp" class="form-text text-muted">Archivo en excel de los estudiantes que ya han solicitado su carné universitario. Descargar de SUNEDU (https://www.carneuniversitario.com.pe  opción: Solicitud Carnés)</small>
                                                </div>





                                                <button type="submit" class="btn btn-primary">Importar</button>
                                            </form>
                                        </div>


                                    </div>
                                    <!--end #tab1 -->
                                    <div class="tab-pane" id="tab2">

                                        <h3>Paso 2 - Normalización de código de estudiante</h3>
                                        <form class="form-inline">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <select class="form-control select" name="expiry-year">

                                                        <option value="17">2017</option>
                                                        <option value="16">2016</option>

                                                    </select>
                                                    <button type="submit" class="btn btn-primary">Buscar</button>
                                                </div>

                                            </div>
                                        </form>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Estudiantes sin codigo de estudiante</div>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th>Codigo Estudiante</th> 
                                                        <th>Facultad</th> 
                                                        <th>Programa</th> 
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> 
                                                        <td>Luis Casiano</td>
                                                        <td></td> 
                                                        <td>Medicina</td> 
                                                        <td>Medicina Forence</td> 
                                                    </tr>
                                                    <tr> 
                                                        <td>Julio Casiano</td>
                                                        <td></td> 
                                                        <td>Medicina</td> 
                                                        <td>Medicina Forence</td> 
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>
                                        <button type="submit" class="btn btn-success">Generar código a estudiante</button>



                                        <br />
                                        <br />
                                        <br />


                                        <form class="form-inline">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <select class="form-control select" name="expiry-year">

                                                        <option value="17">2017</option>
                                                        <option value="16">2016</option>

                                                    </select>
                                                    <button type="submit" class="btn btn-primary">Buscar</button>
                                                </div>

                                            </div>
                                        </form>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Estudiantes</div>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th>Codigo Estudiante</th> 
                                                        <th>Facultad</th> 
                                                        <th>Programa</th> 
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr> 
                                                        <td>Luis Casiano</td>
                                                        <td></td> 
                                                        <td>Medicina</td> 
                                                        <td>Medicina Forence</td> 
                                                    </tr>
                                                    <tr> 
                                                        <td>Julio Casiano</td>
                                                        <td></td> 
                                                        <td>Medicina</td> 
                                                        <td>Medicina Forence</td> 
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>


                                        <button type="submit" class="btn btn-success">Migrar Estudiantes</button>











                                    </div>
                                    <!--end #tab2 -->
                       
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <ul class="pager wizard">
                                <li class="previous disabled"><a class="btn btn-primary btn-round" href="javascript:void(0);">Anterior</a></li>
                                
                                <li class="next"><a class="btn btn-primary btn-round" href="javascript:void(0);">Siguiente</a></li>
                                <li class="btn-round"><a href="/pedidos" class="btn btn-info btn-round" href="javascript:void(0);">Cancelar</a></li>
                                
                            </ul>
                            </form>
                        </div>
                    </div>
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>

                       

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id="chat_compose_wrapper">
        <div class="tippy-top">
            <div class="recipient">Allison Grayce</div>
            <ul class="card-actions icons  right-top">
                <li>
                    <a href="javascript:void(0)">
                        <i class="zmdi zmdi-videocam"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
                        <i class="zmdi zmdi-more-vert"></i>
                    </a>
                    <ul class="dropdown-menu btn-primary dropdown-menu-right">
                        <li>
                            <a href="javascript:void(0)">Option One</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Option Two</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Option Three</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)" data-chat="close">
                        <i class="zmdi zmdi-close"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class='chat-wrapper scrollbar'>
            <div class='chat-message scrollbar'>
                <div class='chat-message chat-message-recipient'>
                    <img class='chat-image chat-image-default' src='assets/img/profiles/05.jpg' />
                    <div class='chat-message-wrapper'>
                        <div class='chat-message-content'>
                            <p>Hey Mike, we have funding for our new project!</p>
                        </div>
                        <div class='chat-details'>
                            <span class='today small'></span>
                        </div>
                    </div>
                </div>
                <div class='chat-message chat-message-sender'>
                    <img class='chat-image chat-image-default' src='assets/img/profiles/02.jpg' />
                    <div class='chat-message-wrapper '>
                        <div class='chat-message-content'>
                            <p>Awesome! Photo booth banh mi pitchfork kickstarter whatever, prism godard ethical 90's cray selvage.</p>
                        </div>
                        <div class='chat-details'>
                            <span class='today small'></span>
                        </div>
                    </div>
                </div>
                <div class='chat-message chat-message-recipient'>
                    <img class='chat-image chat-image-default' src='assets/img/profiles/05.jpg' />
                    <div class='chat-message-wrapper'>
                        <div class='chat-message-content'>
                            <p> Artisan glossier vaporware meditation paleo humblebrag forage small batch.</p>
                        </div>
                        <div class='chat-details'>
                            <span class='today small'></span>
                        </div>
                    </div>
                </div>
                <div class='chat-message chat-message-sender'>
                    <img class='chat-image chat-image-default' src='assets/img/profiles/02.jpg' />
                    <div class='chat-message-wrapper'>
                        <div class='chat-message-content'>
                            <p>Bushwick letterpress vegan craft beer dreamcatcher kickstarter.</p>
                        </div>
                        <div class='chat-details'>
                            <span class='today small'></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer id="compose-footer">
            <form class="form-horizontal compose-form">
                <ul class="card-actions icons left-bottom">
                    <li>
                        <a href="javascript:void(0)">
                            <i class="zmdi zmdi-attachment-alt"></i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="zmdi zmdi-mood"></i>
                        </a>
                    </li>
                </ul>
                <div class="form-group m-10 p-l-75 is-empty">
                    <div class="input-group">
                        <label class="sr-only">Leave a comment...</label>
                        <input type="text" class="form-control form-rounded input-lightGray" placeholder="Leave a comment..">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-blue btn-fab  btn-fab-sm">
                                <i class="zmdi zmdi-mail-send"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </form>
        </footer>
    </section>
</div>
<footer id="footer_wrapper">
    <div class="footer-content">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h6>Want to Work with Us?</h6>
                <p>Paleo flexitarian bushwick letterpress, ea migas yr adipisicing. Man bun tacos tumblr kombucha, yuccie banjo affogato dolore gentrify retro chartreuse. Anim austin tempor ethical, sapiente food truck fanny pack farm-to-table.
                    Culpa keytar esse tilde hoodie, art party nostrud messenger bag authentic helvetica kinfolk cred eu affogato forage.</p>
            </div>
            <div class="col-xs-12 col-sm-2">
                <h6>Company</h6>
                <ul>
                    <li><a href="javascript:void(0)">About Us </a></li>
                    <li><a href="javascript:void(0)">Careers</a></li>
                    <li><a href="javascript:void(0)">Privacy Policy</a></li>
                    <li><a href="javascript:void(0)">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4">
                <h6>Email Newsletters</h6>
                <p>Sign up for new MaterialWrap content, updates, and offers.</p>
                <div class="form-group is-empty">
                    <div class="input-group">
                        <label class="control-label sr-only" for="footerEmail">Email Address</label>
                        <input type="email" class="form-control" id="footerEmail" placeholder="Enter your email address...">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-white btn-fab animate-fab btn-fab-sm">
                                <i class="zmdi zmdi-mail-send"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row copy-wrapper">
            <div class="col-xs-8">
                <p class="copy">&copy; Copyright <time class="year"></time> MaterialWrap - All Rights Reserved</p>
            </div>
            <div class="col-xs-4">
                <ul class="social">
                    <li>
                        <a href="javascript:void(0)"> </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"> </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"> </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"> </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

@endsection


@section('js')


<script type="text/javascript">
    $(document).ready(function () {
       


    });


</script>  


@endsection



