


<div class="portlet light portlet-fit bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class=" icon-layers font-green"></i>
            <span class="caption-subject font-green bold uppercase">Resultado de Proceso</span>

        </div>
    </div>
    <div class="portlet-body">
        <div class="mt-element-list">
            <div class="mt-list-head list-simple ext-1 font-white bg-blue-chambray">
                <div class="list-head-title-container">

                    <h3 class="list-title">Resultados</h3>
                    <div class="caption-desc font-grey-cascade"> Los estudiantes sin foto y con más de un código de alumno asignado no serán considerados en el Archivo Excel (Formato SUNEDU). Para ser considerados, regularice las fotos y/o confirmar un solo carné universitario.</div>
                    
                </div>
            </div>
            <div class="mt-list-container list-simple ext-1 group">
                <a class="list-toggle-container" data-toggle="collapse" href="#archivosGenerados" aria-expanded="false">
                    <div class="list-toggle done uppercase"> Archivos Generados
                        <span class="badge badge-default pull-right bg-white font-green bold">3</span>
                    </div>
                </a>
                <div class="panel-collapse collapse in" id="archivosGenerados">
                    <ul>
                        <li class="mt-list-item done">
                            <div class="list-icon-container">
                                <i class="icon-check"></i>
                            </div>
                            <div class="list-datetime">  </div>
                            <div class="list-item-content">
                                <h3 class="uppercase">
                                    <a href="reportes/fotosEstudiantes.zip">Fotos Estudiantes</a>
                                </h3>
                            </div>
                        </li>
                        <li class="mt-list-item done">
                            <div class="list-icon-container">
                                <i class="icon-check"></i>
                            </div>
                            <div class="list-datetime">  </div>
                            <div class="list-item-content">
                                <h3 class="uppercase">
                                    <a href="reportes/fotosEstudiantesFormatoSUNEDU.zip">Fotos Estudiantes (Formato SUNEDU)</a>
                                </h3>
                            </div>
                        </li>
                        <li class="mt-list-item done">
                            <div class="list-icon-container">
                                <i class="icon-check"></i>
                            </div>
                            <div class="list-datetime">  </div>
                            <div class="list-item-content">
                                <h3 class="uppercase">
                                    <a href="reportes/ArchivoExcelFormatoSUNEDU.xls">Archivo Excel (Formato SUNEDU)</a>
                                </h3>
                            </div>
                        </li>
                    </ul>
                </div>
                <a class="list-toggle-container" data-toggle="collapse" href="#estudiantesSinFoto" aria-expanded="false">
                    <div class="list-toggle uppercase"> Estudiantes sin Foto 
                        <span class="badge badge-default pull-right bg-white font-dark bold">8</span>
                    </div>
                </a>
                <div class="panel-collapse collapse" id="estudiantesSinFoto">
                    <ul>

                        <li class="mt-list-item done">
                            <div class="list-icon-container">
                             
                            </div>
                          
                            <div class="list-item-content">
                                <h3 class="uppercase">
                                    <a href="#">Número Documento</a>
                                </h3>
                            </div>
                        </li>
                        @foreach ($estudiantesPedidoNoTieneFoto as $estudiantePedidoNoTieneFoto)

                        <li class="mt-list-item done">
                            <div class="list-icon-container">
                                <i class="icon-check"></i>
                            </div>
                            <div class="list-datetime">  </div>
                            <div class="list-item-content">
                                <h3 class="uppercase">
                                    <a href="#">{{ $estudiantePedidoNoTieneFoto->nrodocumento }}</a>
                                </h3>
                            </div>
                        </li>
                        @endforeach





                    </ul>
                </div>

                <a class="list-toggle-container" data-toggle="collapse" href="#estudiantesConDocumentoDuplicado" aria-expanded="false">
                    <div class="list-toggle uppercase"> Estudiantes con más de un código de alumno
                        <span class="badge badge-default pull-right bg-white font-dark bold">1</span>
                    </div>
                </a>
                <div class="panel-collapse collapse" id="estudiantesConDocumentoDuplicado">
                    <ul>
                        <li class="mt-list-item done">
                            <div class="list-icon-container">
                               
                            </div>
                          
                            <div class="list-item-content">
                                <h3 class="uppercase">
                                    <a href="#">Número Documento</a>
                                </h3>
                            </div>
                        </li>

                        @foreach ($estudiantesPedidoNrodocumentoDuplicado as $estudiantePedidoNrodocumentoDuplicado)

                        <li class="mt-list-item done">
                            <div class="list-icon-container">
                                <i class="icon-check"></i>
                            </div>
                            <div class="list-datetime">  </div>
                            <div class="list-item-content">
                                <h3 class="uppercase">
                                    <a href="#">{{ $estudiantePedidoNrodocumentoDuplicado->nrodocumento }}</a>
                                </h3>
                            </div>
                        </li>
                        @endforeach





                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>












