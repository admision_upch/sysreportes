<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_Perfil extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_perfiles';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['perfil_id', 'user_id'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function perfil() {
        return $this->belongsTo('App\Perfil');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

}
