<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudiantesPedido extends Model {

    protected $connection = 'pgsql_syscarnes';
    protected $table = 'estudiantes_pedido';

}
