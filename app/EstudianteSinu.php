<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstudianteSinu extends Model {

    protected $connection = 'pgsql_syscarnes';
    protected $table = 'estudiantes_sinu';

}
