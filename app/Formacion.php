<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formacion extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'formaciones';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function facultades() {
        return $this->hasMany('App\Facultad');
    }
        public function formaciones() {
        return $this->hasMany('App\Formacion');
    }

}
