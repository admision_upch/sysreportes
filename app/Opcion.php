<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Opcion extends Model {

    protected $connection = 'pgsql_sysseguridad_seguridad';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'opciones';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'enlace', 'destino', 'nivel', 'tipoenlace', 'padre', 'orden', 'imagen', 'estado', 'sistema_id'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function sistema() {
        return $this->belongsTo('App\Sistema');
    }

    public function acceso() {
        return $this->hasMany('App\Acceso');
    }

}
