<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Acceso extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accesos';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['perfil_id', 'opcion_id'];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function opcion() {
        return $this->belongsTo('App\Opcion');
    }

    public function perfil() {
        return $this->belongsTo('App\Perfil');
    }

}
