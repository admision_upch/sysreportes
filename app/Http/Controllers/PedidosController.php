<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\FacultadSinu;
use App\FacultadSunedu;
use App\ProgramaSinu;
use App\ProgramaSunedu;
use App\EstudianteSinu;
use App\EstudiantesPedido;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\componentes\FormulariosController;
use View;
use Excel;
use Intervention\Image\ImageManagerStatic as Image;
use Chumper\Zipper\Zipper;

class PedidosController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $codfacultad = '0';
        return view('pedidos.index', compact('codfacultad'));
    }

    public function indexFacultades($codfacultad) {
        return view('pedidos.index', compact('codfacultad'));
    }

    public function indexDetalle($pedido_id) {
        $codfacultad = '0';

        return view('pedidos.detallepedido', compact('pedido_id', 'codfacultad'));
    }

    public function datatablePedidosResumen() {
        $PedidosResumen = DB::connection('pgsql_syscarnes')->select("select * from vst_resumen_pedidos");
        return Datatables::of($PedidosResumen)
                        ->addColumn('action', function ($pedido) {
                            return '<a onclick="verDetallePedido(' . $pedido->id . ')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i>Ver Detalle</a><a href="#edit-' . $pedido->id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                        })
                        ->make(true);
    }

    public function validarPedidos(Request $request) {
        $data = DB::connection('pgsql_syscarnes')->select("select * from vst_pedido_exporta_confirmados");
        $data = json_decode(json_encode($data), true);

        $fotosPersona = glob("fotos/personas/*.jpg");
        $fotosCarne = glob("fotos/carne/*.jpg");
        /* Eliminar las fotos de la carpeta fotoscarne */
        array_map('unlink', $fotosPersona);
        array_map('unlink', $fotosCarne);

        foreach ($data as $index => $dato) {
            $fotoRepositorio = RepositorioFotosPersonas . $dato['docu_num'] . ".jpg";
            $fotoPersona = "fotos//personas//" . $dato['docu_num'] . ".jpg";
            $fotoCarne = "fotos//carne//014_" . $dato['cod_est'] . $dato['docu_num'] . ".jpg";

            if (file_exists($fotoRepositorio)) {
                copy($fotoRepositorio, $fotoPersona);
                copy($fotoRepositorio, $fotoCarne);
                $img = Image::make($fotoPersona);
                $img->fit(240, 288);
                $img->save();
            } else {
                $estudiantesPedido = EstudiantesPedido::where([
                            ['codalumno', '=', $dato['cod_est']]
                        ])->first();
                $estudiantesPedido->notienefoto = '1';
                $estudiantesPedido->save();
            }
        }

        $zipper = new Zipper();
        $zipper->make('reportes/fotosEstudiantes.zip')->add($fotosPersona)->close();
        $zipper->make('reportes/fotosEstudiantesFormatoSUNEDU.zip')->add($fotosCarne)->close();
        $this->downloadExcelConfirmados('xls');

        return response()->json(['status' => 'success', 'message' => 'Proceso ejecutado exitosamente', 'data' => view('pedidos.detallepedido')], 200);
    }

    public function viewArchivosGenerados() {
        $pedido_id = 1;
        $estudiantesPedidoNoTieneFoto = EstudiantesPedido::where([
                    ['estado', '=', '1'],
                    ['pedido_id', '=', $pedido_id],
                    ['notienefoto', '=', '1'],
                ])->get();

        $estudiantesPedidoNrodocumentoDuplicado = DB::connection('pgsql_syscarnes')->select("select distinct nrodocumento from vst_estudiantes_pedido_nrodocumento_duplicado");

        return view('pedidos.view_archivosgenerados', compact('estudiantesPedidoNoTieneFoto', 'estudiantesPedidoNrodocumentoDuplicado'));
    }

    public function downloadExcelConfirmados($type) {
        $data = DB::connection('pgsql_syscarnes')->select("select * from vst_pedido_exporta_confirmados");
        $data = json_decode(json_encode($data), true);
        $archivo = Excel::create('ArchivoExcelFormatoSUNEDU', function($excel) use ($data) {
                    $dataoriginal = $data;
                    $m = 500;
                    $k = 0;
                    for ($ii = 1; $ii < 2; ++$ii) {
                        $data = array_slice($data, $k, $m);
                        $excel->sheet('Archivo de Carga ' . $ii, function($sheet) use ($data) {
                            $sheet->setStyle(array(
                                'font' => array(
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                )
                            ));
                            $sheet->mergeCells('A1:D1');
                            $sheet->row(1, array(
                                'SUNEDU'
                            ));
                            $sheet->mergeCells('A2:D2');
                            $sheet->row(2, array(
                                'SISTEMA DE EMISIÓN Y EXPEDICIÓN DE CARNÉS UNIVERSITARIOS'
                            ));
                            $sheet->mergeCells('G1:J1');
                            $sheet->cell('G1', function($cell) {
                                $cell->setValue("014");
                            });
                            $sheet->mergeCells('G2:J2');
                            $sheet->cell('G2', function($cell) {
                                $cell->setValue("UNIVERSIDAD PERUANA CAYETANO HEREDIA");
                            });
                            $sheet->cell('E2', function($cell) {
                                $cell->setValue('ARCHIVO DE CARGA');
                            });
                            $sheet->cell('F1', function($cell) {
                                $cell->setValue('Codigo Universidad');
                            });
                            $sheet->cell('F2', function($cell) {
                                $cell->setValue('Universidad');
                            });
                            $sheet->cell('A4:N4', function($cell) {
                                $cell->setBackground('#1F497D');
                                $cell->setFontColor('#ffffff');
                            });
                            $ultimaFila = count($data) + 4;
                            $sheet->setBorder('A1:N' . $ultimaFila, 'thin');
                            $i = 0;
                            foreach ($data as $index => $dato) {
                                $nombre_fichero = "fotos//carne//014_" . $dato['cod_est'] . $dato['docu_num'] . ".jpg";
                                if (file_exists($nombre_fichero)) {

                                    $sheet->row($i + 5, array(
                                        $dato['cod_univ'], $dato['cod_est'], $dato['apepat'], $dato['apemat'], $dato['nombre'], $dato['fac_nom_esc_pos'], $dato['abr_fac_abr_pos'], $dato['esc_carr_esp_pos'], $dato['abr_carr_abr_esp_pos'], $dato['docu_tip'], $dato['docu_num'], $dato['ano_ing'], $dato['per_ing'], $dato['fil']
                                    ));

                                    $i++;
                                }
                            }

                            $sheet->row(4, [
                                'COD_UNIV', 'COD_EST', 'APEPAT', 'APEMAT', 'NOMBRE', 'FAC_NOM-ESC_POS', 'ABR_FAC-ABR_POS', 'ESC_CARR-ESP_POS', 'ABR_CARR-ABR_ESP_POS', 'DOCU_TIP', 'DOCU_NUM', 'AÑO_ING', 'PER_ING', 'FIL'
                            ]);

                            $sheet->cells('A5:A' . $ultimaFila, function($cells) {
                                $cells->setBackground('#A6A6A6');
                            });
                            $sheet->cells('G5:G' . $ultimaFila, function($cells) {
                                $cells->setBackground('#A6A6A6');
                            });
                            $sheet->cells('I5:I' . $ultimaFila, function($cells) {
                                $cells->setBackground('#A6A6A6');
                            });
                            $sheet->cells('K5:K' . $ultimaFila, function($cells) {
                                $cells->setAlignment('right');
                            });
                            $sheet->cells('A4:N4', function($cells) {
                                $cells->setAlignment('center');
                            });
                            $sheet->setColumnFormat(array(
                                'F5:F' . $ultimaFila => '0',
                                'J5:J' . $ultimaFila => '0',
                            ));
                        });

                        $k = $m + 1;
                        $m = $m + 500;
                    }

                    $data = array_slice($dataoriginal, $k, count($dataoriginal));

                    $excel->sheet('Archivo de Carga ' . $ii, function($sheet) use ($data) {
                        $sheet->setStyle(array(
                            'font' => array(
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            )
                        ));

                        $sheet->mergeCells('A1:D1');
                        $sheet->row(1, array(
                            'SUNEDU'
                        ));
                        $sheet->mergeCells('A2:D2');
                        $sheet->row(2, array(
                            'SISTEMA DE EMISIÓN Y EXPEDICIÓN DE CARNÉS UNIVERSITARIOS'
                        ));

                        $sheet->mergeCells('G1:J1');


                        $sheet->cell('G1', function($cell) {
                            $cell->setValue("014");
                        });

                        $sheet->mergeCells('G2:J2');

                        $sheet->cell('G2', function($cell) {
                            $cell->setValue("UNIVERSIDAD PERUANA CAYETANO HEREDIA");
                        });

                        $sheet->cell('E2', function($cell) {
                            $cell->setValue('ARCHIVO DE CARGA');
                        });
                        $sheet->cell('F1', function($cell) {
                            $cell->setValue('Codigo Universidad');
                        });

                        $sheet->cell('F2', function($cell) {
                            $cell->setValue('Universidad');
                        });

                        $sheet->cell('A4:N4', function($cell) {
                            $cell->setBackground('#1F497D');
                            $cell->setFontColor('#ffffff');
                        });


                        $ultimaFila = count($data) + 4;


                        $sheet->setBorder('A1:N' . $ultimaFila, 'thin');

                        $i = 0;
                        foreach ($data as $index => $dato) {

                            $nombre_fichero = "fotos//carne//014_" . $dato['cod_est'] . $dato['docu_num'] . ".jpg";
                            if (file_exists($nombre_fichero)) {

                                $sheet->row($i + 5, array(
                                    $dato['cod_univ'], $dato['cod_est'], $dato['apepat'], $dato['apemat'], $dato['nombre'], $dato['fac_nom_esc_pos'], $dato['abr_fac_abr_pos'], $dato['esc_carr_esp_pos'], $dato['abr_carr_abr_esp_pos'], $dato['docu_tip'], $dato['docu_num'], $dato['ano_ing'], $dato['per_ing'], $dato['fil']
                                ));

                                $i++;
                            }
                        }

                        $sheet->row(4, [
                            'COD_UNIV', 'COD_EST', 'APEPAT', 'APEMAT', 'NOMBRE', 'FAC_NOM-ESC_POS', 'ABR_FAC-ABR_POS', 'ESC_CARR-ESP_POS', 'ABR_CARR-ABR_ESP_POS', 'DOCU_TIP', 'DOCU_NUM', 'AÑO_ING', 'PER_ING', 'FIL'
                        ]);

                        $sheet->cells('A5:A' . $ultimaFila, function($cells) {
                            $cells->setBackground('#A6A6A6');
                        });
                        $sheet->cells('G5:G' . $ultimaFila, function($cells) {
                            $cells->setBackground('#A6A6A6');
                        });
                        $sheet->cells('I5:I' . $ultimaFila, function($cells) {
                            $cells->setBackground('#A6A6A6');
                        });
                        $sheet->cells('K5:K' . $ultimaFila, function($cells) {
                            $cells->setAlignment('right');
                        });
                        $sheet->cells('A4:N4', function($cells) {
                            $cells->setAlignment('center');
                        });
                        $sheet->setColumnFormat(array(
                            'F5:F' . $ultimaFila => '0',
                            'J5:J' . $ultimaFila => '0',
                        ));
                    });
                })->store($type, 'reportes/');
        return response()->json(['status' => 'success', 'message' => 'Proceso ejecutado exitosamente', 'data' => ''], 200);
    }

}
