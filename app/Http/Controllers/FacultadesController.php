<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Facultad;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;

class FacultadesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $facultades = Facultad::where([
                    ['eliminado', '=', null]
                ])->get();

        return view('backEnd.facultades.index', compact('facultades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        $modalidades = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from modalidades");
        $formaciones = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from formaciones");
        $formularios = new FormulariosController();
        $selectModalidades = $formularios->selectSimpleNoLabel($modalidades, 'modalidad_id', '0');
        $selectFormaciones = $formularios->selectSimpleNoLabel($formaciones, 'formacion_id', '0');
        return view('backEnd.facultades.create', compact('selectModalidades', 'selectFormaciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, ['id_dependencia' => 'required', 'cod_dependencia' => 'required', 'name' => 'required', 'modalidad_id' => 'required', 'formacion_id' => 'required',]);

        Facultad::create($request->all());

        Session::flash('message', 'Facultade added!');
        Session::flash('status', 'success');

        return redirect('facultades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id) {
        $facultade = Facultad::findOrFail($id);

        return view('backEnd.facultades.show', compact('facultade'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id) {
        $facultade = Facultad::findOrFail($id);
        $modalidades = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from modalidades");
        $formaciones = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from formaciones");
        $formularios = new FormulariosController();
        $selectModalidades = $formularios->selectSimpleNoLabel($modalidades, 'modalidad_id', $facultade->modalidad_id);
        $selectFormaciones = $formularios->selectSimpleNoLabel($formaciones, 'formacion_id', $facultade->formacion_id);
        return view('backEnd.facultades.edit', compact('facultade', 'selectModalidades', 'selectFormaciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request) {
        $this->validate($request, ['id_dependencia' => 'required', 'cod_dependencia' => 'required', 'name' => 'required', 'modalidad_id' => 'required', 'formacion_id' => 'required',]);

        $facultade = Facultad::findOrFail($id);
        $facultade->update($request->all());

        Session::flash('message', 'Facultade updated!');
        Session::flash('status', 'success');

        return redirect('facultades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id) {
        $facultade = Facultad::findOrFail($id);

        $facultade->eliminado = '1';
        $facultade->save();

        Session::flash('message', 'Facultade deleted!');
        Session::flash('status', 'success');

        return redirect('facultades');
    }

    public function selectByModalidadAndFormacion($modalidad, $formacion) {

        $facultades = DB::connection('pgsql_sysseguridad_seguridad')->select("select distinct id_dependencia as cod,  name from facultades where modalidad_id=" . $modalidad . " and formacion_id=" . $formacion);
        $formularios = new FormulariosController();
        $selectFacultades = $formularios->selectSimpleNoLabel($facultades, 'id_dependencia', '0');
        return $selectFacultades;
    }

}
