<?php

namespace App\Http\Controllers\componentes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormulariosController extends Controller {

    public function selectSimple($data, $label, $name, $seleccionado) {
        return view('componentes.component_select', compact('data', 'label', 'name', 'seleccionado'));
    }

    public function selectSimpleNoLabel($data, $name, $seleccionado) {
        return view('componentes.component_select_no_label', compact('data', 'name', 'seleccionado'));
    }

}
