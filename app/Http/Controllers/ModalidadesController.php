<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modalidad;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class ModalidadesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $modalidades = Modalidad::where([
                    ['eliminado', '=', null]
                ])->get();

        return view('backEnd.modalidades.index', compact('modalidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.modalidades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        Modalidad::create($request->all());

        Session::flash('message', 'Modalidade added!');
        Session::flash('status', 'success');

        return redirect('modalidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $modalidade = Modalidad::findOrFail($id);

        return view('backEnd.modalidades.show', compact('modalidade'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $modalidade = Modalidad::findOrFail($id);

        return view('backEnd.modalidades.edit', compact('modalidade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $modalidade = Modalidad::findOrFail($id);
        $modalidade->update($request->all());

        Session::flash('message', 'Modalidade updated!');
        Session::flash('status', 'success');

        return redirect('modalidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $modalidade = Modalidad::findOrFail($id);

        $modalidade->eliminado = '1';
        $modalidade->save();

        Session::flash('message', 'Modalidade deleted!');
        Session::flash('status', 'success');

        return redirect('modalidades');
    }

}
