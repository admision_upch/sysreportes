<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Formacion;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class FormacionesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $formaciones = Formacion::where([
                    ['eliminado', '=', null]
                ])->get();

        return view('backEnd.formaciones.index', compact('formaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backEnd.formaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        Formacion::create($request->all());

        Session::flash('message', 'Formacione added!');
        Session::flash('status', 'success');

        return redirect('formaciones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $formacione = Formacion::findOrFail($id);

        return view('backEnd.formaciones.show', compact('formacione'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $formacione = Formacion::findOrFail($id);

        return view('backEnd.formaciones.edit', compact('formacione'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $formacione = Formacion::findOrFail($id);
        $formacione->update($request->all());

        Session::flash('message', 'Formacione updated!');
        Session::flash('status', 'success');

        return redirect('formaciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $formacione = Formacion::findOrFail($id);

        $formacione->eliminado = '1';
        $formacione->save();

        Session::flash('message', 'Formacione deleted!');
        Session::flash('status', 'success');

        return redirect('formaciones');
    }

}
