<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Acceso;
use App\Perfil;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;

class AccesosController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $perfiles = Perfil::where([
                    ['eliminado', '=', null]
                ])->get();



        return view('backEnd.accesos.index', compact('perfiles'));
    }

    public function accesos_tree() {
        $accesos = DB::connection('pgsql_sysseguridad_seguridad')->select("select cast (o.id  as character varying) as id, case when padre is null then '#' else cast(padre  as character varying) end as parent,  o.name as text,'' as icon,   case when a.perfil_id>1 then 1 else 0 end as state    from seguridad.opciones o left join 
seguridad.accesos a on o.id=a.opcion_id and a.perfil_id=6
left join seguridad.perfiles p on p.id=a.perfil_id 
where o.sistema_id=2 and (o.eliminado is null or o.eliminado='') and o.estado='1' 
");
        foreach ($accesos as $d) {
            $id = $d->state;
            if ($d->state == '1') {
                $d->state = array(
                    'selected' => true,
                    'checked' => true,
                    'disabled' => false,
                    'opened' => true,
                );
            } else {
                $d->state = array(
                    'selected' => false,
                    'checked' => false,
                    'disabled' => false,
                    'opened' => true,
                );
            }
        }
        return response()->json($accesos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $perfiles = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from perfiles");
        $opciones = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from opciones");
        $sistemas = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from sistemas");



        $formularios = new FormulariosController();
        $selectPerfiles = $formularios->selectSimpleNoLabel($perfiles, 'perfil_id', '0');
        $selectOpciones = $formularios->selectSimpleNoLabel($opciones, 'opcion_id', '0');
        $selectSistemas = $formularios->selectSimpleNoLabel($sistemas, 'sistema_id', '0');
        return view('backEnd.accesos.create', compact('selectPerfiles', 'selectOpciones', 'selectSistemas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, ['perfil_id' => 'required', 'opcion_id' => 'required',]);

        Acceso::create($request->all());

        Session::flash('message', 'Acceso added!');
        Session::flash('status', 'success');

        return redirect('accesos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id) {
        $acceso = Acceso::findOrFail($id);
        //  dd($acceso->perfil);
        return view('backEnd.accesos.show', compact('acceso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id) {
        $acceso = Acceso::findOrFail($id);
        $perfiles = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from perfiles");
        $opciones = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from opciones");
        $formularios = new FormulariosController();
        $selectPerfiles = $formularios->selectSimpleNoLabel($perfiles, 'perfil_id', $acceso->perfil_id);
        $selectOpciones = $formularios->selectSimpleNoLabel($opciones, 'opcion_id', $acceso->opcion_id);
        return view('backEnd.accesos.edit', compact('acceso', 'selectPerfiles', 'selectOpciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request) {
        $this->validate($request, ['perfil_id' => 'required', 'opcion_id' => 'required',]);

        $acceso = Acceso::findOrFail($id);
        $acceso->update($request->all());

        Session::flash('message', 'Acceso updated!');
        Session::flash('status', 'success');

        return redirect('accesos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id) {
        $acceso = Acceso::findOrFail($id);
        $acceso->eliminado = '1';
        $acceso->save();
        Session::flash('message', 'Acceso deleted!');
        Session::flash('status', 'success');
        return redirect('accesos');
    }

}
