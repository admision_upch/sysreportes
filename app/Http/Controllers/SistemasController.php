<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Sistema;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class SistemasController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $sistemas = Sistema::where([
                    ['eliminado', '=', null]
                ])->get();
        
       // dd($sistemas);
        return view('backEnd.sistemas.index', compact('sistemas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('backEnd.sistemas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {

        Sistema::create($request->all());

        Session::flash('message', 'Sistema added!');
        Session::flash('status', 'success');

        return redirect('sistemas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id) {
        $sistema = Sistema::findOrFail($id);

        return view('backEnd.sistemas.show', compact('sistema'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id) {
        $sistema = Sistema::findOrFail($id);

        return view('backEnd.sistemas.edit', compact('sistema'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request) {

        $sistema = Sistema::findOrFail($id);
        $sistema->update($request->all());

        Session::flash('message', 'Sistema updated!');
        Session::flash('status', 'success');

        return redirect('sistemas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id) {
        $sistema = Sistema::findOrFail($id);

        $sistema->eliminado = '1';
        $sistema->save();

        Session::flash('message', 'Sistema deleted!');
        Session::flash('status', 'success');

        return redirect('sistemas');
    }

}
