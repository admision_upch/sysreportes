<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $users = User::where([
                    ['eliminado', '=', null]
                ])->get();

        return view('backEnd.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('backEnd.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {

        User::create($request->all());

        Session::flash('message', 'User added!');
        Session::flash('status', 'success');

        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id) {
        $user = User::findOrFail($id);

        return view('backEnd.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id) {
        $user = User::findOrFail($id);

        return view('backEnd.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request) {

        $user = User::findOrFail($id);
        $user->update($request->all());

        Session::flash('message', 'User updated!');
        Session::flash('status', 'success');

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id) {
        $user = User::findOrFail($id);

        $user->eliminado = '1';
        $user->save();


        Session::flash('message', 'User deleted!');
        Session::flash('status', 'success');

        return redirect('users');
    }

}
