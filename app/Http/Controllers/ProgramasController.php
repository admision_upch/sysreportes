<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Programa;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;


class ProgramasController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $programas = Programa::where([
                    ['eliminado', '=', null]
                ])->get();

        return view('backEnd.programas.index', compact('programas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //$facultades = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from modalidades");
        $modalidades = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from modalidades");
        $formaciones = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from formaciones");
        $facultades = DB::connection('pgsql_sysseguridad_seguridad')->select("select distinct id_dependencia as cod,  name from facultades where id='-1'");
        $formularios = new FormulariosController();
        $selectModalidades = $formularios->selectSimpleNoLabel($modalidades, 'modalidad_id', '0');
        $selectFormaciones = $formularios->selectSimpleNoLabel($formaciones, 'formacion_id', '0');
        $selectFacultades = $formularios->selectSimpleNoLabel($facultades, 'id_dependencia', '0');
        return view('backEnd.programas.create', compact('selectModalidades', 'selectFormaciones','selectFacultades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, ['id_dependencia' => 'required', 'modalidad_id' => 'required', 'formacion_id' => 'required', 'cod' => 'required', 'name' => 'required',]);

        Programa::create($request->all());

        Session::flash('message', 'Programa added!');
        Session::flash('status', 'success');

        return redirect('programas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id) {
        $programa = Programa::findOrFail($id);
//echo $programa->facultad;
      //  dd($programa->facultad);

        return view('backEnd.programas.show', compact('programa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id) {
        $programa = Programa::findOrFail($id);
  
        $modalidades = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from modalidades");
        $formaciones = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from formaciones");
        $facultades = DB::connection('pgsql_sysseguridad_seguridad')->select("select distinct id_dependencia as cod,  name from facultades");
        $formularios = new FormulariosController();
        $selectModalidades = $formularios->selectSimpleNoLabel($modalidades, 'modalidad_id', $programa->modalidad_id);
        $selectFormaciones = $formularios->selectSimpleNoLabel($formaciones, 'formacion_id', $programa->formacion_id);
        $selectFacultades = $formularios->selectSimpleNoLabel($facultades, 'id_dependencia', $programa->id_dependencia);
        return view('backEnd.programas.edit', compact('programa', 'selectModalidades', 'selectFormaciones', 'selectFacultades'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request) {
        $this->validate($request, ['id_dependencia' => 'required', 'modalidad_id' => 'required', 'formacion_id' => 'required', 'cod' => 'required', 'name' => 'required',]);

        $programa = Programa::findOrFail($id);
        $programa->update($request->all());

        Session::flash('message', 'Programa updated!');
        Session::flash('status', 'success');

        return redirect('programas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id) {
        $programa = Programa::findOrFail($id);
        $programa->eliminado = '1';
        $programa->save();

        Session::flash('message', 'Programa deleted!');
        Session::flash('status', 'success');

        return redirect('programas');
    }

}
