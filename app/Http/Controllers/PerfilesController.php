<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Perfil;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;

class PerfilesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $perfiles = Perfil::where([
                    ['eliminado', '=', null]
                ])->get();

        return view('backEnd.perfiles.index', compact('perfiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $sistemas = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from sistemas");
        $formularios = new FormulariosController();
        $selectSistemas = $formularios->selectSimpleNoLabel($sistemas, 'sistema_id', '0');
        return view('backEnd.perfiles.create', compact('selectSistemas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, ['name' => 'required',]);

        Perfil::create($request->all());

        Session::flash('message', 'Perfile added!');
        Session::flash('status', 'success');

        return redirect('perfiles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id) {
        $perfile = Perfil::findOrFail($id);

        return view('backEnd.perfiles.show', compact('perfile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id) {
        $perfile = Perfil::findOrFail($id);
        $sistemas = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from sistemas");
        $formularios = new FormulariosController();
        $selectSistemas = $formularios->selectSimpleNoLabel($sistemas, 'sistema_id', $perfile->sistema_id);
        return view('backEnd.perfiles.edit', compact('perfile', 'selectSistemas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request) {
        $this->validate($request, ['name' => 'required',]);

        $perfile = Perfil::findOrFail($id);
        $perfile->update($request->all());

        Session::flash('message', 'Perfile updated!');
        Session::flash('status', 'success');

        return redirect('perfiles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id) {
        $perfile = Perfil::findOrFail($id);

        $perfile->eliminado = '1';
        $perfile->save();

        Session::flash('message', 'Perfile deleted!');
        Session::flash('status', 'success');

        return redirect('perfiles');
    }

}
