<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User_Perfil;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;

class Users_PerfilesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $users_perfiles = User_Perfil::all();

        return view('backEnd.users_perfiles.index', compact('users_perfiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $perfiles = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from perfiles");
        $users = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod, username as name from users");
        $formularios = new FormulariosController();
        $selectPerfiles = $formularios->selectSimpleNoLabel($perfiles, 'perfil_id', '0');
        $selectUsers = $formularios->selectSimpleNoLabel($users, 'user_id', '0');

        return view('backEnd.users_perfiles.create', compact('selectPerfiles', 'selectUsers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, ['perfil_id' => 'required', 'user_id' => 'required',]);

        User_Perfil::create($request->all());

        Session::flash('message', 'Users_Perfile added!');
        Session::flash('status', 'success');

        return redirect('users_perfiles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id) {
        $users_perfile = User_Perfil::findOrFail($id);

        return view('backEnd.users_perfiles.show', compact('users_perfile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id) {
        $users_perfile = Users_Perfile::findOrFail($id);

        return view('backEnd.users_perfiles.edit', compact('users_perfile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request) {
        $this->validate($request, ['perfil_id' => 'required', 'user_id' => 'required',]);

        $users_perfile = Users_Perfile::findOrFail($id);
        $users_perfile->update($request->all());

        Session::flash('message', 'Users_Perfile updated!');
        Session::flash('status', 'success');

        return redirect('users_perfiles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id) {
        $users_perfile = Users_Perfile::findOrFail($id);

        $users_perfile->delete();

        Session::flash('message', 'Users_Perfile deleted!');
        Session::flash('status', 'success');

        return redirect('users_perfiles');
    }

}
