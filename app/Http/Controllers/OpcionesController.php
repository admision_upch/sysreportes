<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Opcion;
use App\Sistema;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\componentes\FormulariosController;

class OpcionesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $sistemas = Sistema::where([
                    ['eliminado', '=', null]
                ])->get();

        return view('backEnd.opciones.index', compact('sistemas'));
    }

    public function opciones($id) {

        $sistema = Sistema::findOrFail($id);
        return view('backEnd.sistemas.opciones', compact('sistema'));
    }

    public function opciones_tree() {
        $accesos = DB::connection('pgsql_sysseguridad_seguridad')->select("select cast (o.id  as character varying) as id, case when padre is null then '#' else cast(padre  as character varying) end as parent,  o.name as text,'' as icon,   case when a.perfil_id>1 then 1 else 0 end as state    from seguridad.opciones o left join 
seguridad.accesos a on o.id=a.opcion_id and a.perfil_id=2
left join seguridad.perfiles p on p.id=a.perfil_id 
where o.sistema_id=2 and (o.eliminado is null or o.eliminado='') and o.estado='1' order by orden
");
        foreach ($accesos as $d) {
            $id = $d->state;
            //$d->text=$d->text.' <a href="javascript:;" onclick="formeditable()" id="firstname'.$d->id.'" > sss</a>';
            if ($d->state == '1') {
                $d->state = array(
                    'selected' => true,
                    'checked' => true,
                    'disabled' => false,
                    'opened' => true,
                );
            } else {
                $d->state = array(
                    'selected' => false,
                    'checked' => false,
                    'disabled' => false,
                    'opened' => true,
                );
            }
        }
        return response()->json($accesos);
    }

    public function opciones_tree_crud_create_node(Request $request) {

        try {
            $opcione = new Opcion();
            $opcione->padre = $request->padre;
            $opcione->orden = $request->orden;
            $opcione->name = $request->name;
            $opcione->sistema_id = $request->sistema_id;
            $opcione->save();

            return response()->json(['status' => 'success', 'message' => 'Creacion ejecutada exitosamente', 'id' => $opcione->id], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'danger', 'message' => 'Creacion no ejecutada', 'id' => ''], 500);
        }
    }

    public function opciones_tree_crud_rename_node(Request $request) {

        try {
            $opcione = Opcion::findOrFail($request->id);
            $opcione->name = $request->name;
            $opcione->save();

            return response()->json(['status' => 'success', 'message' => 'Actualizacion ejecutada exitosamente', 'id' => $opcione->id], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'danger', 'message' => 'Actualizacion no ejecutada', 'id' => ''], 500);
        }
    }
    public function opciones_tree_crud_delete_node(Request $request) {

        try {
            $opcione = Opcion::findOrFail($request->id);
            $opcione->eliminado = 1;
            $opcione->save();

            return response()->json(['status' => 'success', 'message' => 'Eliminación ejecutada exitosamente', 'id' => $opcione->id], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'danger', 'message' => 'Eliminación no ejecutada', 'id' => ''], 500);
        }
    }
    public function opciones_tree_crud() {

        if (isset($_GET['operation'])) {
            $fs = new tree(db::get('mysqli://root@127.0.0.1/test'), array('structure_table' => 'tree_struct', 'data_table' => 'tree_data', 'data' => array('nm')));
            try {
                $rslt = null;
                switch ($_GET['operation']) {
                    /*  case 'analyze':
                      var_dump($fs->analyze(true));
                      die();
                      break;
                      case 'get_node':
                      $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int) $_GET['id'] : 0;
                      $temp = $fs->get_children($node);
                      $rslt = array();
                      foreach ($temp as $v) {
                      $rslt[] = array('id' => $v['id'], 'text' => $v['nm'], 'children' => ($v['rgt'] - $v['lft'] > 1));
                      }
                      break;
                      case "get_content":
                      $node = isset($_GET['id']) && $_GET['id'] !== '#' ? $_GET['id'] : 0;
                      $node = explode(':', $node);
                      if (count($node) > 1) {
                      $rslt = array('content' => 'Multiple selected');
                      } else {
                      $temp = $fs->get_node((int) $node[0], array('with_path' => true));
                      $rslt = array('content' => 'Selected: /' . implode('/', array_map(function ($v) {
                      return $v['nm'];
                      }, $temp['path'])) . '/' . $temp['nm']);
                      }
                      break; */
                    case 'create_node':
                        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int) $_GET['id'] : 0;
                        $temp = $fs->mk($node, isset($_GET['position']) ? (int) $_GET['position'] : 0, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'New node'));
                        $rslt = array('id' => $temp);
                        break;
                    case 'rename_node':
                        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int) $_GET['id'] : 0;
                        $rslt = $fs->rn($node, array('nm' => isset($_GET['text']) ? $_GET['text'] : 'Renamed node'));
                        break;
                    case 'delete_node':
                        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int) $_GET['id'] : 0;
                        $rslt = $fs->rm($node);
                        break;
                    case 'move_node':
                        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int) $_GET['id'] : 0;
                        $parn = isset($_GET['parent']) && $_GET['parent'] !== '#' ? (int) $_GET['parent'] : 0;
                        $rslt = $fs->mv($node, $parn, isset($_GET['position']) ? (int) $_GET['position'] : 0);
                        break;
                    case 'copy_node':
                        $node = isset($_GET['id']) && $_GET['id'] !== '#' ? (int) $_GET['id'] : 0;
                        $parn = isset($_GET['parent']) && $_GET['parent'] !== '#' ? (int) $_GET['parent'] : 0;
                        $rslt = $fs->cp($node, $parn, isset($_GET['position']) ? (int) $_GET['position'] : 0);
                        break;
                    default:
                        throw new Exception('Unsupported operation: ' . $_GET['operation']);
                        break;
                }
                // header('Content-Type: application/json; charset=utf-8');
                //echo json_encode($rslt);
                return response()->json($rslt);
            } catch (Exception $e) {
                header($_SERVER["SERVER_PROTOCOL"] . ' 500 Server Error');
                header('Status:  500 Server Error');
                echo $e->getMessage();
            }
            die();
        }

        //return response()->json($accesos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $sistemas = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from sistemas");
        $formularios = new FormulariosController();
        $selectSistemas = $formularios->selectSimpleNoLabel($sistemas, 'sistema_id', '0');
        return view('backEnd.opciones.create', compact('selectSistemas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request) {

        Opcion::create($request->all());

        Session::flash('message', 'Opcion added!');
        Session::flash('status', 'success');

        return redirect('opciones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id) {
        $opcione = Opcion::findOrFail($id);
        //dd($opcione->sistema->name);
        return view('backEnd.opciones.show', compact('opcione'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id) {
        $opcione = Opcion::findOrFail($id);
        $sistemas = DB::connection('pgsql_sysseguridad_seguridad')->select("select id as cod,  name from sistemas");
        $formularios = new FormulariosController();
        $selectSistemas = $formularios->selectSimpleNoLabel($sistemas, 'sistema_id', $opcione->sistema_id);
        return view('backEnd.opciones.edit', compact('opcione', 'selectSistemas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request) {

        $opcione = Opcion::findOrFail($id);
        $opcione->update($request->all());

        Session::flash('message', 'Opcion updated!');
        Session::flash('status', 'success');

        return redirect('opciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id) {
        $opcione = Opcion::findOrFail($id);
        $opcione->eliminado = '1';
        $opcione->save();


        Session::flash('message', 'Opcion deleted!');
        Session::flash('status', 'success');

        return redirect('opciones');
    }

}
