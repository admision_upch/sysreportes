<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */



Route::get('/', function () {
    return redirect('/login');
})->middleware('auth');



Route::get('/facultades', 'FacultadesController@index');
Route::get('/pedidos', 'PedidosController@index');
Route::get('/pedidos/detalle/{id}', 'PedidosController@indexDetalle')->name('detallepedido');

Route::get('/pedidos/facultad/{codfacultad}', 'PedidosController@indexFacultades');

Route::get('/pedidos/facultadsinu/edit/{pedido_id}/{codfacultad}/{codmodalidad}/{niv_formacion}', 'FacultadesController@editFacultadSinu');
Route::put('/pedidos/facultadsinu/update', 'FacultadesController@updateFacultadSinu');

Route::get('/pedidos/programasinu/edit/{pedido_id}/{codfacultad}/{codprograma}', 'ProgramasController@editProgramaSinu');
Route::put('/pedidos/programasinu/update', 'ProgramasController@updateProgramaSinu');

Route::get('/pedidosresumen/datatable', 'PedidosController@datatablePedidosResumen')->name('datatable.pedidosresumen');
Route::get('/facultadessinuresumen/datatable/{codfacultad}', 'FacultadesController@datatableFacultadesSinuResumen')->name('datatable.facultadessinuresumen');
Route::get('/estudiantespedido/datatable/{codfacultad}/{codmodalidad}', 'EstudiantesController@datatableEstudiantesPedido')->name('datatable.estudiantespedido');
Route::get('/facultadessinu/datatable/{pedido_id}/{codfacultad}', 'FacultadesController@datatableFacultadesSinu')->name('datatable.facultadessinu');
Route::get('/programassinu/datatable/{codfacultad}', 'ProgramasController@datatableProgramasSinu')->name('datatable.programassinu');


Route::get('viewArchivosGenerados', 'PedidosController@viewArchivosGenerados');
Route::get('validarPedidos', 'PedidosController@validarPedidos');


//Route::get('/facultadessinu/datatable', 'FacultadesController@datatableFacultadesSinu')->name('datatable.facultadessinu');
//Route::get('/programassinu/datatable', 'FacultadesController@datatableProgramasSinu')->name('datatable.programassinu');
//Route::get('/estudiantessinu/datatable', 'FacultadesController@datatableEstudiantesSinu')->name('datatable.estudiantessinu');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('inicio');






Route::group(['middleware' => ['auth']], function () {
    Route::resource('perfiles', 'PerfilesController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('accesos', 'AccesosController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('accesos', 'AccesosController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('users_perfiles', 'Users_PerfilesController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('facultades', 'FacultadesController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('opciones', 'OpcionesController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('sistemas', 'SistemasController');
    // Route::resource('opciones_tree_crud_create_node', 'OpcionesController');
    Route::get('sistemas/{id}/opciones', 'OpcionesController@opciones')->name('opciones');
    Route::get('sistemas/{id}/opciones_tree', 'OpcionesController@opciones_tree')->name('opciones_tree');
   // Route::get('opciones/tree/create_node/{sistema_id}/{padre}/{orden}/{name}', 'OpcionesController@opciones_tree_crud_create_node')->name('opciones_tree_crud_create_node');
    
});
Route::post('opciones_tree_crud_create_node', 'OpcionesController@opciones_tree_crud_create_node')->name('opciones_tree_crud_create_node');
Route::post('opciones_tree_crud_rename_node', 'OpcionesController@opciones_tree_crud_rename_node')->name('opciones_tree_crud_rename_node');
Route::post('opciones_tree_crud_delete_node', 'OpcionesController@opciones_tree_crud_delete_node')->name('opciones_tree_crud_delete_node');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('users', 'UsersController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('modalidades', 'ModalidadesController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('formaciones', 'FormacionesController');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('programas', 'ProgramasController');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('accesos_tree', 'AccesosController@accesos_tree')->name('accesos_tree');
});

Route::get('/facultadesselect/modalidad/{modalidad_id}/formacion/{formacion_id}', 'FacultadesController@selectByModalidadAndFormacion');



