<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccesosTable extends Migration {

    /**
     * Run the migrations.
     * php artisan crud:generate Accesos --fields="name:string:required,perfil_id:integer:required, opcion_id:integer:required" --route=yes --pk=id
     * @return void
     */
    
    public function up() {
        Schema::create('accesos', function (Blueprint $table) {
            $table->increments('id');
            $table->char('estado', 1)->nullable()->default('1');
            $table->char('eliminado', 1)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('perfil_id')->unsigned();
            $table->foreign('perfil_id')->references('id')->on('perfiles');
            $table->integer('opcion_id')->unsigned();
            $table->foreign('opcion_id')->references('id')->on('opciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('accesos');
    }

}
