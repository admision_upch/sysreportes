<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('programas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod');
            $table->string('name');
            $table->char('estado', 1)->nullable()->default('1');
            $table->char('eliminado', 1)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('id_dependencia');
            $table->integer('facultad_id')->unsigned();
            $table->integer('modalidad_id')->unsigned();
            $table->integer('formacion_id')->unsigned();
            $table->foreign('facultad_id')->references('id')->on('facultades');
            $table->foreign('modalidad_id')->references('id')->on('modalidades');
            $table->foreign('formacion_id')->references('id')->on('formaciones');
            $table->unique(['cod', 'id_dependencia', 'modalidad_id', 'formacion_id']);
            //$table->foreign(['id_dependencia', 'modalidad_id', 'formacion_id'])->references(['id_dependencia', 'modalidad_id', 'formacion_id'])->on('facultades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('programas');
    }

}
