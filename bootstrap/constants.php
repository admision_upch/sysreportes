<?php

if (env('APP_ENV') === 'local') {
    define('RepositorioFotosPersonas', 'C://xampp//fotos_pedido_2017//');
}


if (env('APP_ENV') === 'production') {
    define('RepositorioFotosPersonas', '/mnt/fotos$');
}